var isMobile = false;
var checkMobile = function(){
    if(Modernizr.mq('only screen and (max-width: 1023px)')){
        isMobile=true;
        $('nav#mm-menu').mmenu({zposition :"front",clone: true}).removeClass('main-nav');
    }else{
        if(!isMobile) return;
        $('nav#mm-menu').mmenu({zposition :"front",clone: true}).addClass('main-nav');
    }


}
$(function() {
    checkMobile();
    $("#login-button").on('click',function(e){
        e.preventDefault();
        $("#box-login").slideToggle(400);
    });
    $(document).on('load mousemove resize', function() {
        currentWidth = $(window).width();
        $('#resize').html($(window).width() + ' x ' + $(document).height());
    });
    
    $(document).on('resize', function() {
        checkMobile();
    });        
    

     $('.modal').popup({
        outline: true,
        transition: 'all 0.3s'
    });
    $('.close-modal').click(function(e){
        e.preventDefault();
        $('.modal').popup('hide');
    });
    
    $('.rate-btn-box').on('click',function(e){
        e.preventDefault();
        
        $('.rate-box').fadeIn();
    });
    
    
    
    /**
        Botão de configuraçẽos da página de fornecedores
    **/
    
    $(".config-button").bind('click',function(e){
        $(this).toggleClass('active');
        $(".config-box").slideToggle();
    });
    
    
    /***
        Botão fake que submete o formulario de orçamento;
    */
    
    $("#submit-budget-request").on('click',function(e){
        e.preventDefault();
        $("#form-request-budget").closest('.modal').popup('hide');
        $("#modal-request-budget-success").popup('show');
    });
});